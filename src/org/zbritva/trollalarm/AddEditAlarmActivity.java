package org.zbritva.trollalarm;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

class CreateController{
	static boolean firstCreate =true;
}

public class AddEditAlarmActivity extends Activity {
	private TextView text_openDialog;
	private TextView text_alarmTime;
	private TextView text_methodName;
	private TextView text_alarmSound;
	private Button bt_accep;
	private Button bt_reject;
	private Button bt_changeMethod;
	private Button bt_changeSound;
	private ListView list_weekDays;
	private int DIALOG_TIME = 1;
	private int DIALOG_METHOD = 2;
	private int currentHour = 7;
	private int currentMinute = 0;
	private Alarm alarm;
	private final String[] methodsName = {"SimpleShutdown", "Random"};
	//private int changedMethodId;
	private final int PICKFILE_RESULT_CODE = 1;
	WeekDays week;
	List<DayOfWeek> weekDays;
	private OnClickListener open_timePickerDialog = new OnClickListener() {
	
		public void onClick(View v) {
			showDialog(DIALOG_TIME);
		}
	};
	
	protected Dialog onCreateDialog(int id) {
	      if (id == DIALOG_TIME) {
	        TimePickerDialog tpd = new TimePickerDialog(this, CallBack, currentHour, currentMinute, true);
	        return tpd;
	      }
	      if (id == DIALOG_METHOD){
	    	  Toast.makeText(getApplicationContext(), "METHOD", Toast.LENGTH_SHORT).show();
	    	  AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	  builder.setTitle("Change method");
	    	  
	    	  builder.setItems(methodsName, new DialogInterface.OnClickListener() {
					
					public void onClick(DialogInterface dialog, int which) {
						//changedMethodId = which;
						text_methodName.setText(methodsName[which]);
						alarm.setMethodName(methodsName[which]);
						Toast.makeText(getApplicationContext(), "Get "+methodsName[which]+" method", Toast.LENGTH_SHORT).show();
					}
				});
	    	  
	    	  builder.setCancelable(false);
	    	  return builder.create();
	      }
	      return super.onCreateDialog(id);
	    }
	
	OnTimeSetListener CallBack = new OnTimeSetListener() {

		public void onTimeSet(TimePicker view, int hour, int minute) {
			 currentHour = hour;
			 currentMinute = minute; 
			 alarm.setHour(currentHour);
			 alarm.setMinute(currentMinute);
		     text_alarmTime.setText(currentHour + ":" + currentMinute);
		}
	  };
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_edit_alarm);
		
		//if (CreateController.firstCreate)
		//{
			Log.d("ACTIVITY","FIRST CREATE");
			text_openDialog = (TextView) findViewById(R.id.text_openDialog);
			text_openDialog.setOnClickListener(open_timePickerDialog);
		
			text_alarmTime = (TextView) findViewById(R.id.text_alarmTime);
			text_methodName = (TextView) findViewById(R.id.text_methodName);
			text_alarmSound = (TextView) findViewById(R.id.text_alarmSound);
		if (getIntent().getAction() == "org.zbritva.intent.action.editalarm"){
			alarm = (Alarm) getIntent().getExtras().getSerializable("alarm");
			currentHour = alarm.getHour();
			currentMinute = alarm.getMinute();
			text_alarmTime.setText(currentHour + ":" + currentMinute);
			text_methodName.setText(alarm.getMethodName());
			//if (alarm.getSoundPath().length()!=0)
			{
				//String str[] = alarm.getSoundPath().split("\\");
			    text_alarmSound.setText(alarm.getSoundPath());
			}
		}
		if (getIntent().getAction() == "org.zbritva.intent.action.addalarm"){
			alarm = new Alarm();
		}
		
		Intent intent = new Intent();
		setResult(0, intent);
		
		bt_accep = (Button) findViewById(R.id.bt_accept);
		bt_reject = (Button) findViewById(R.id.bt_reject);
		
		bt_accep.setOnClickListener(bt_accepClick_listener);
		bt_reject.setOnClickListener(bt_rejectClick_listener);
		
		bt_changeMethod = (Button) findViewById(R.id.bt_changeMethod);
		bt_changeSound = (Button) findViewById(R.id.bt_changeSound);
		
		bt_changeMethod.setOnClickListener(bt_changeMethodClick_listener);
		bt_changeSound.setOnClickListener(bt_changeSoundClick_listener);
		list_weekDays = (ListView) findViewById(R.id.list_weekDays);
		weekDays = new ArrayList<DayOfWeek>();
		week = alarm.getWeek();
		
		DayOfWeek day = new DayOfWeek(0);
		day.setEnabled(week.isMonday());
		weekDays.add(day);
		
		day = new DayOfWeek(1);
		day.setEnabled(week.isTuesday());
		weekDays.add(day);
		
		day = new DayOfWeek(2);
		day.setEnabled(week.isWednesday());
		weekDays.add(day);
		
		day = new DayOfWeek(3);
		day.setEnabled(week.isThursday());
		weekDays.add(day);
		
		day = new DayOfWeek(4);
		day.setEnabled(week.isFriday());
		weekDays.add(day);
		
		day = new DayOfWeek(5);
		day.setEnabled(week.isSaturday());
		weekDays.add(day);
		
		day = new DayOfWeek(6);
		day.setEnabled(week.isSunday());
		weekDays.add(day);
		
		list_weekDays.setAdapter(new InteractiveWeekDaysAdaptor(this,weekDays));
		CreateController.firstCreate = false;
		//}
	}
	
	OnClickListener bt_changeSoundClick_listener = new OnClickListener() {
		
		public void onClick(View arg0) {
			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		    	intent.setType("file/*");
		    startActivityForResult(intent,PICKFILE_RESULT_CODE);
		}
	};
	
	OnClickListener bt_accepClick_listener = new OnClickListener() {
		
		public void onClick(View v) {
			Intent intent = new Intent();
			for(int i=0; i<7;i++){
				if (weekDays.get(i).name == "Monday")
					week.setMonday(weekDays.get(i).isEnabled());
				if (weekDays.get(i).name == "Tuesday")
					week.setTuesday(weekDays.get(i).isEnabled());
				if (weekDays.get(i).name == "Wednesday")
					week.setWednesday(weekDays.get(i).isEnabled());
				if (weekDays.get(i).name == "Thursday")
					week.setThursday(weekDays.get(i).isEnabled());
				if (weekDays.get(i).name == "Friday")
					week.setFriday(weekDays.get(i).isEnabled());
				if (weekDays.get(i).name == "Saturday")
					week.setSaturday(weekDays.get(i).isEnabled());
				if (weekDays.get(i).name == "Sunday")
					week.setSunday(weekDays.get(i).isEnabled());
			}
			
			//alarm.setWeek(week);
		    intent.putExtra("alarm", alarm);
		    if (getIntent().getAction() == "org.zbritva.intent.action.addalarm"){
		    	setResult(RESULT_FIRST_USER+1, intent);
		    }
		    if (getIntent().getAction() == "org.zbritva.intent.action.editalarm"){
		    	setResult(RESULT_FIRST_USER+2, intent);
		    }
		    finish();
		}
	};
	
	OnClickListener bt_rejectClick_listener = new OnClickListener() {
		
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.putExtra("alarm", alarm);
			setResult(RESULT_CANCELED, intent);
			finish();
		}
	};
	
	OnClickListener bt_changeMethodClick_listener = new OnClickListener() {
		
		public void onClick(View v) {
			showDialog(DIALOG_METHOD);
		}
	};
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	Log.d("ACTIVITY RESLT", "CALL "+requestCode+" result "+resultCode);
    	if (requestCode != 3){
    		if(resultCode==RESULT_OK){
    		    String FilePath = data.getData().getPath();
    		    Log.d("ACTIVITY RESULT FOR SOUND", FilePath);
    		    
    		    //if (alarm.getSoundPath().length()!=0)
    		    {
	    			//String str[] = FilePath.split("\\");
	    		    alarm.setSoundPath(FilePath);
	    		    text_alarmSound.setText(alarm.getSoundPath());
    		    }
    		return;
    		}
    	}
    }
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent intent = new Intent();
		intent.putExtra("alarm", alarm);
		setResult(RESULT_CANCELED, intent);
		finish();
	}
}
