package org.zbritva.trollalarm;

import android.widget.ArrayAdapter;
import java.util.List;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;


public class InteractiveWeekDaysAdaptor extends ArrayAdapter<DayOfWeek> {
	private final List<DayOfWeek> list;
    private final Activity context;
    ListView listView;

    public InteractiveWeekDaysAdaptor(Activity context, List<DayOfWeek> list) {
        super(context, R.layout.checkboxonlylayout, list);
        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected CheckBox checkbox;
    }

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.checkboxonlylayout, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.checkbox = (CheckBox) view.findViewById(R.id.checkWeekDay);
           	viewHolder.checkbox.setText(list.get(position).name);
			
            viewHolder.checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					DayOfWeek element = (DayOfWeek) viewHolder.checkbox.getTag();
                    try{
						for(int i=0; i<7;i++){
							if(list.get(i).getName() == element.getName()){
								list.get(i).setEnabled(isChecked);
							}
						}
						notifyDataSetChanged();
                    } catch (ArrayIndexOutOfBoundsException e){
                    	Log.d("INDEX", "INVALID INDEX");
                    }
				}
			});
   
            
            view.setTag(viewHolder);
            viewHolder.checkbox.setTag(list.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkbox.setTag(list.get(position));
        }
        ViewHolder holder = (ViewHolder) view.getTag();

        holder.checkbox.setChecked(list.get(position).isEnabled());
        return view;
    }
}
