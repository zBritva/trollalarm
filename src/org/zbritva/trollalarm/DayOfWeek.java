package org.zbritva.trollalarm;

import java.io.Serializable;

public class DayOfWeek implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7516152592159385153L;
	String name;
	boolean enabled = false;
	
	public DayOfWeek(String name) {
		this.name = name;
	}
	
	public DayOfWeek(int day) {
		switch (day) {
		case 0:
			name = "Monday";
			break;
		case 1:
			name = "Tuesday";
			break;
		case 2:
			name = "Wednesday";
			break;
		case 3:
			name = "Thursday";
			break;
		case 4:
			name = "Friday";
			break;
		case 5:
			name = "Saturday";
			break;
		case 6:
			name = "Sunday";
			break;
		
		default:
			break;
		}
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setName(int day){
		switch (day) {
		case 0:
			name = "Monday";
			break;
		case 1:
			name = "Tuesday";
			break;
		case 2:
			name = "Wednesday";
			break;
		case 3:
			name = "Thursday";
			break;
		case 4:
			name = "Friday";
			break;
		case 5:
			name = "Saturday";
			break;
		case 6:
			name = "Sunday";
			break;
		
		default:
			break;
		}
	}
	
	public void setEnabled(boolean enabled){
		this.enabled = enabled;
	}
	
	public boolean isEnabled(){
		return enabled;
	}
	
	public String getName(){
		return name;
	}
	
	@Override
	public boolean equals(Object o) {
		if (super.equals(0)){
			DayOfWeek d= (DayOfWeek) o;
			if (d.enabled == enabled && d.name == name){
				return true;
			}
		}
		return false;
	}
}
