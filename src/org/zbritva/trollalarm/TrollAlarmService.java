package org.zbritva.trollalarm;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class TrollAlarmService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override 
	public void onCreate(){
		Toast.makeText(this, "TrollAlarmService created", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onDestroy(){
		Toast.makeText(this, "TrollAlarmService destroyed", Toast.LENGTH_SHORT).show();
	}
	
	@Override 
	public void onStart(Intent intent, int startId){
		Toast.makeText(this, "TrollAlarmService started", Toast.LENGTH_SHORT).show();
	}

}
