package org.zbritva.trollalarm;

import java.io.Serializable;

import android.util.Log;

public class WeekDays implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5378581745324704742L;
	/**
	 * 
	 */
	private boolean Monday =false;
	private boolean Tuesday =false;
	private boolean Wednesday =false;
	private boolean Thursday =false;
	private boolean Friday =false;
	private boolean Saturday =false;
	private boolean Sunday =false;
	
	public void setMonday(boolean enabled){
		Monday = enabled;
	}
	
	public void setTuesday(boolean enabled){
		Tuesday = enabled;
	}
	
	public void setWednesday(boolean enabled){
		Wednesday = enabled;
	}
	
	public void setThursday(boolean enabled){
		Thursday = enabled;
	}
	public void setFriday(boolean enabled){
		Friday = enabled;
	}
	public void setSaturday(boolean enabled){
		Saturday = enabled;
	}
	public void setSunday(boolean enabled){
		Sunday = enabled;
	}
	
	public boolean isMonday(){
		return Monday;
	}
	
	public boolean isTuesday(){
		return Tuesday;
	}
	
	public boolean isWednesday(){
		return Wednesday;
	}
	
	public boolean isThursday(){
		return Thursday;
	}
	
	public boolean isFriday(){
		return Friday;
	}
	
	public boolean isSaturday(){
		return Saturday;
	}
	
	public boolean isSunday(){
		return Sunday;
	}
	
	
	@Override
	public boolean equals(Object o) {
		Log.d("EQUALS", "EQUALS ALARMS");
		if (super.equals(o) ==true){
			WeekDays a = (WeekDays) o;
				if(this.Friday == a.Friday)
					if(this.Monday ==a.Monday)
						if(this.Saturday ==a.Saturday)
							if(this.Sunday == a.Sunday)
								if(this.Thursday == a.Thursday)
									if(this.Tuesday ==a.Tuesday)
										if(this.Wednesday ==a.Wednesday){
											Log.d("EQUALS", "EQUALS TRUE");
												return true;
										}
		}
		Log.d("EQUALS", "EQUALS FALSE");
		return false;
	}
}
