package org.zbritva.trollalarm;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class AlarmListActivity extends Activity {
	//TODO Need test all functions of client side program
	//add, remove, reset alarms, display alarms (layout) and other
	ListView list_alarms;
	Button bt_addAlarm;
	Activity context;
	//Intent intentEditAlarm;
	Intent intentAddAlarm;
	ArrayAdapter<Alarm> adapter;
	List<Alarm> list;
	int currentAlarmPositionForEdit;
	int requestCode = 1;
	//Need for work with database
	DBHelper dbHelper;
	SQLiteDatabase db;
	public void setCurrentAlarmPositionForEdit(int currentAlarmPositionForEdit){
		this.currentAlarmPositionForEdit = currentAlarmPositionForEdit;
	}


	public void onClickStartService(){
		startService(new Intent(this, TrollAlarmService.class));
	}

	OnClickListener bt_addAlarm_listener = new OnClickListener() {

		public void onClick(View v) {
			startActivityForResult(intentAddAlarm, requestCode);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm_list);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();

		list_alarms = (ListView) findViewById(R.id.list_alarms);
		bt_addAlarm = (Button) findViewById(R.id.bt_addAlarm);
		bt_addAlarm.setOnClickListener(bt_addAlarm_listener);
		context = this;
		intentAddAlarm = new Intent("org.zbritva.intent.action.addalarm");
		//intentEditAlarm = new Intent("org.zbritva.intent.action.editalarm");


		list = new ArrayList<Alarm>();
		//query all dates from table
		Log.d("DATABASE", "QUERY ALL DATA IN TABLE");
		Cursor curs = db.query(DBHelper.ALARM_TABLE, null, null, null, null, null, null);
		Log.d("DATABASE", "ROW COUNT IN TABLE: " + curs.getCount());
		if (curs.moveToFirst()) {
			Log.d("DATABASE", "MOVE TO FIRST");
			// ���������� ������ �������� �� ����� � �������
			int idColIndex = curs.getColumnIndex(DBHelper.ALARM_ID);
			int enableColIndex =curs.getColumnIndex(DBHelper.ALARM_ENABLED);
			int hourColIndex = curs.getColumnIndex(DBHelper.ALARM_HOUR);
			int minuteColIndex = curs.getColumnIndex(DBHelper.ALARM_MINUTE);
			int methodColIndex = curs.getColumnIndex(DBHelper.ALARM_METHOD_NAME);
			int soundPathColIndex = curs.getColumnIndex(DBHelper.ALARM_SOUND_PATH);

			int mondayColIndex = curs.getColumnIndex(DBHelper.WEEK_DAY_MONDAY);
			int tuesdayColIndex = curs.getColumnIndex(DBHelper.WEEK_DAY_TUESDAY);
			int wednesdayColIndex = curs.getColumnIndex(DBHelper.WEEK_DAY_WEDNESDAY);
			int thursdayColIndex = curs.getColumnIndex(DBHelper.WEEK_DAY_THURSDAY);
			int fridayColIndex =curs.getColumnIndex(DBHelper.WEEK_DAY_FRIDAY);
			int saturdayColIndex =curs.getColumnIndex(DBHelper.WEEK_DAY_SATURDAY);
			int sundayColIndex = curs.getColumnIndex(DBHelper.WEEK_DAY_SUNDAY);

			do {
				//Create container item Alarm
				Log.d("DATABASE", "LOAD ALARM FROM DATABASE");
				Log.d("DATABASE", "ALARM ID: "+curs.getInt(idColIndex));
				Alarm alarm = new Alarm();
				// get alarm values and set to container item
				alarm.setID(curs.getInt(idColIndex));
				if (curs.getInt(enableColIndex) == 1) 
					alarm.setEnabled(true);
				else
					alarm.setEnabled(false);
				alarm.setHour(curs.getInt(hourColIndex));
				alarm.setMinute(curs.getInt(minuteColIndex));
				alarm.setMethodName(curs.getString(methodColIndex));
				alarm.setSoundPath(curs.getString(soundPathColIndex));
				Log.d("DATABASE", "MONDAY IS "+curs.getString(mondayColIndex));
				if (curs.getInt(mondayColIndex) == 1) 
					alarm.getWeek().setMonday(true);
				else
					alarm.getWeek().setMonday(false);

				if (curs.getInt(tuesdayColIndex) == 1)
					alarm.getWeek().setTuesday(true);
				else
					alarm.getWeek().setTuesday(false);
				
				if (curs.getInt(wednesdayColIndex) == 1)
					alarm.getWeek().setWednesday(true);
				else
					alarm.getWeek().setWednesday(false);
				
				if (curs.getInt(thursdayColIndex) == 1)
					alarm.getWeek().setThursday(true);
				else
					alarm.getWeek().setThursday(false);
				
				if(curs.getInt(fridayColIndex) == 1)
					alarm.getWeek().setFriday(true);
				else
					alarm.getWeek().setFriday(false);
				
				if (curs.getInt(saturdayColIndex) == 1)
					alarm.getWeek().setSaturday(true);
				else
					alarm.getWeek().setSaturday(false);

				
				if (curs.getInt(sundayColIndex) == 1)
					alarm.getWeek().setSunday(true);
				else
					alarm.getWeek().setSunday(false);

				//add alarm to list for view
				Log.d("DATABASE", "ADD ALARM TO LIST");
				list.add(alarm);
				//move to next row
				//if next row is not, then leave loop
			} while (curs.moveToNext());
		}

		adapter  = new InteractiveAlarmsAdapter(context, list,db);

		list_alarms.setAdapter(adapter);
		//list_alarms.setOnItemLongClickListener(list_EditItem_listener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_alarm_list, menu);
		return true;
	}    

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d("ACTIVITY RESLT", "CALL "+requestCode+" result "+resultCode);
		if (requestCode != 1){
			return;
		}
		if (data == null) {
			return;
		}
		if (data.getExtras() != null){
			Alarm alarm = (Alarm) data.getExtras().getSerializable("alarm");
			if (alarm == null){
				Log.d("ACTIVITY RESLT", "RETURNED ALARM IS NULL");

			}
			//1 - add new alarm to list
			if (resultCode == RESULT_FIRST_USER+1){
				Log.d("DATABASE", "ADD ALARM TO DATABASE");
				//add alarm to database
				ContentValues cv = new ContentValues();
				//Connect to database
				//SQLiteDatabase db = dbHelper.getWritableDatabase();

				cv.putNull(DBHelper.ALARM_ID);
				cv.put(DBHelper.ALARM_ENABLED, alarm.isEnabled());
				cv.put(DBHelper.ALARM_HOUR, alarm.getHour());
				cv.put(DBHelper.ALARM_MINUTE, alarm.getMinute());
				cv.put(DBHelper.ALARM_METHOD_NAME, alarm.getMethodName());
				cv.put(DBHelper.ALARM_SOUND_PATH, alarm.getSoundPath());
				cv.put(DBHelper.WEEK_DAY_MONDAY, alarm.getWeek().isMonday());
				cv.put(DBHelper.WEEK_DAY_TUESDAY, alarm.getWeek().isTuesday());
				cv.put(DBHelper.WEEK_DAY_WEDNESDAY, alarm.getWeek().isWednesday());
				cv.put(DBHelper.WEEK_DAY_THURSDAY, alarm.getWeek().isThursday());
				cv.put(DBHelper.WEEK_DAY_FRIDAY, alarm.getWeek().isFriday());
				cv.put(DBHelper.WEEK_DAY_SATURDAY, alarm.getWeek().isSaturday());
				cv.put(DBHelper.WEEK_DAY_SUNDAY, alarm.getWeek().isSunday());

				long rowID = db.insert(DBHelper.ALARM_TABLE, null, cv);
				alarm.setID(rowID);
				Log.d("DATABASE", "NEW ALARM ROW ID: "+rowID);
				list.add(alarm);
				adapter.notifyDataSetChanged();
			}
			//2 - update alarm
			if (resultCode == RESULT_FIRST_USER+2){
				Log.d("DATABASE", "REPLASE ALARM");
				//replease alarm in database
				ContentValues cv = new ContentValues();
				//Connect to database
				//SQLiteDatabase db = dbHelper.getWritableDatabase();

				cv.put(DBHelper.ALARM_ID, alarm.getID());
				cv.put(DBHelper.ALARM_ENABLED, alarm.isEnabled());
				cv.put(DBHelper.ALARM_HOUR, alarm.getHour());
				cv.put(DBHelper.ALARM_MINUTE, alarm.getMinute());
				cv.put(DBHelper.ALARM_METHOD_NAME, alarm.getMethodName());
				cv.put(DBHelper.ALARM_SOUND_PATH, alarm.getSoundPath());
				cv.put(DBHelper.WEEK_DAY_MONDAY, alarm.getWeek().isMonday());
				cv.put(DBHelper.WEEK_DAY_TUESDAY, alarm.getWeek().isTuesday());
				cv.put(DBHelper.WEEK_DAY_WEDNESDAY, alarm.getWeek().isWednesday());
				cv.put(DBHelper.WEEK_DAY_THURSDAY, alarm.getWeek().isThursday());
				cv.put(DBHelper.WEEK_DAY_FRIDAY, alarm.getWeek().isFriday());
				cv.put(DBHelper.WEEK_DAY_SATURDAY, alarm.getWeek().isSaturday());
				cv.put(DBHelper.WEEK_DAY_SUNDAY, alarm.getWeek().isSunday());

				db.delete(DBHelper.ALARM_TABLE, DBHelper.ALARM_ID+"="+alarm.getID(), null);
				long rowID = db.insert(DBHelper.ALARM_TABLE, null, cv);	 
				Log.d("DATABASE", "REPLASED ALARM ROW ID: "+rowID);
				alarm.setID(rowID);
				list.set(currentAlarmPositionForEdit, alarm);
				adapter.notifyDataSetChanged();
			}
		}
	}
}
