package org.zbritva.trollalarm;

import java.util.List;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class InteractiveAlarmsAdapter extends ArrayAdapter<Alarm> {
	private final List<Alarm> list;
    private final Activity context;
    ListView listView;
    private SQLiteDatabase db;

    public InteractiveAlarmsAdapter(Activity context, List<Alarm> list, SQLiteDatabase db) {
        super(context, R.layout.rowcheckboxlayout, list);
        this.context = context;
        this.list = list;
        this.db = db;
    }

    static class ViewHolder {
        protected TextView hour;
        protected TextView divaider;
        protected TextView minutes;
        protected TextView weekDays;
        protected CheckBox checkbox;
        protected ImageView imageRemoveAlarm;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.rowcheckboxlayout, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.hour = (TextView) view.findViewById(R.id.hour);
            viewHolder.minutes = (TextView) view.findViewById(R.id.minute);
            viewHolder.divaider = (TextView) view.findViewById(R.id.divaider);
            viewHolder.weekDays = (TextView) view.findViewById(R.id.weekDays);
            viewHolder.imageRemoveAlarm = (ImageView) view.findViewById(R.id.imageRemoveAlarm);
            viewHolder.checkbox = (CheckBox) view.findViewById(R.id.checkAlarm);
            
            viewHolder.checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Alarm alarm = (Alarm) viewHolder.checkbox.getTag();
                    alarm.setEnabled(buttonView.isChecked());
                    Log.d("DATABASE", "IS CHECKED: "+isChecked);
                    //replease alarm in database
    				ContentValues cv = new ContentValues();
    				//Connect to database
    				//SQLiteDatabase db = dbHelper.getWritableDatabase();

    				cv.put(DBHelper.ALARM_ID, alarm.getID());
    				cv.put(DBHelper.ALARM_ENABLED, alarm.isEnabled());
    				cv.put(DBHelper.ALARM_HOUR, alarm.getHour());
    				cv.put(DBHelper.ALARM_MINUTE, alarm.getMinute());
    				cv.put(DBHelper.ALARM_METHOD_NAME, alarm.getMethodName());
    				cv.put(DBHelper.ALARM_SOUND_PATH, alarm.getSoundPath());
    				cv.put(DBHelper.WEEK_DAY_MONDAY, alarm.getWeek().isMonday());
    				cv.put(DBHelper.WEEK_DAY_TUESDAY, alarm.getWeek().isTuesday());
    				cv.put(DBHelper.WEEK_DAY_WEDNESDAY, alarm.getWeek().isWednesday());
    				cv.put(DBHelper.WEEK_DAY_THURSDAY, alarm.getWeek().isThursday());
    				cv.put(DBHelper.WEEK_DAY_FRIDAY, alarm.getWeek().isFriday());
    				cv.put(DBHelper.WEEK_DAY_SATURDAY, alarm.getWeek().isSaturday());
    				cv.put(DBHelper.WEEK_DAY_SUNDAY, alarm.getWeek().isSunday());
    				Log.d("DATABASE", "DELETE ALARM ID: "+alarm.getID());
    				db.delete(DBHelper.ALARM_TABLE, DBHelper.ALARM_ID+"="+alarm.getID(), null);
    				long rowID = db.insert(DBHelper.ALARM_TABLE, null, cv);	 
    				Log.d("DATABASE", "REPLASED ALARM ROW ID: "+rowID);
    				alarm.setID(rowID);
				}
			});
            
            viewHolder.imageRemoveAlarm.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					Alarm element = (Alarm) viewHolder.checkbox.getTag();
					db.delete(DBHelper.ALARM_TABLE, DBHelper.ALARM_ID+"="+element.getID(), null);
					
					try{
						int index = list.indexOf(element);
						if ( list.remove(index) !=null ){
							notifyDataSetChanged();
						}
					}
					catch(ArrayIndexOutOfBoundsException e){
						Log.d("INDEX", "INVALID INDEX");
					}
				}
			});
            OnClickListener click_editAlarm = new OnClickListener() {
            	
            	public void onClick(View v) {
            		Alarm element = (Alarm) viewHolder.checkbox.getTag();
            		try{
            			int index = list.indexOf(element);
            			Log.d("INDEX", "CURRENT INDEX "+index);
            			Intent intentEditAlarm = new Intent("org.zbritva.intent.action.editalarm");
            			intentEditAlarm.putExtra("alarm", list.get(index));
            			AlarmListActivity ala = (AlarmListActivity) context;
            			ala.setCurrentAlarmPositionForEdit(index);
            			context.startActivityForResult(intentEditAlarm, 1);
            		}
            		catch(ArrayIndexOutOfBoundsException e){
            			Log.d("INDEX", "INVALID INDEX");
            		}
            	}
	
            };
            
            viewHolder.hour.setOnClickListener(click_editAlarm);
            viewHolder.minutes.setOnClickListener(click_editAlarm);
            viewHolder.divaider.setOnClickListener(click_editAlarm);
            viewHolder.weekDays.setOnClickListener(click_editAlarm);
            
            view.setTag(viewHolder);
            viewHolder.checkbox.setTag(list.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkbox.setTag(list.get(position));
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        //TODO Need to fix a time display. For example, from 6:0 to 6:00 Example 0:0. Need fix to: 00:00
        holder.hour.setText(String.valueOf(list.get(position).getHour()));
        holder.minutes.setText(String.valueOf(list.get(position).getMinute()));

        String weekDays="";
        if(list.get(position).getWeek().isMonday()){
        	weekDays += "Monday ";
        }
        if(list.get(position).getWeek().isTuesday()){
        	weekDays += "Tuesday ";
        }
        if(list.get(position).getWeek().isWednesday()){
        	weekDays += "Wednesday ";
        }
        if(list.get(position).getWeek().isThursday()){
        	weekDays += "Thursday ";
        }
        if(list.get(position).getWeek().isFriday()){
        	weekDays += "Friday ";
        }
        if(list.get(position).getWeek().isSaturday()){
        	weekDays += "Saturday ";
        }
        if(list.get(position).getWeek().isSunday()){
        	weekDays += "Sunday ";
        }
        holder.weekDays.setText(weekDays);
        
        holder.checkbox.setChecked(list.get(position).isEnabled());
        return view;
    }
}

