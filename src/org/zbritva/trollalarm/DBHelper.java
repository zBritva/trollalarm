package org.zbritva.trollalarm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	// DB const
	// ��
	static final String DB_NAME = "trollalarm";
	static final int DB_VERSION = 1;

	// table name
	static final String ALARM_TABLE = "alarms";

	// filds or columns
	static final String ALARM_ID = "_id";
	static final String ALARM_ENABLED = "enabled";
	static final String ALARM_METHOD_NAME = "method";
	static final String ALARM_SOUND_PATH = "path";
	static final String ALARM_HOUR = "hour";
	static final String ALARM_MINUTE = "minute";
	static final String WEEK_DAY_MONDAY = "monday";
	static final String WEEK_DAY_TUESDAY = "tuesday";
	static final String WEEK_DAY_WEDNESDAY = "wednesday";
	static final String WEEK_DAY_THURSDAY = "thursday";
	static final String WEEK_DAY_FRIDAY = "friday";
	static final String WEEK_DAY_SATURDAY = "saturday";
	static final String WEEK_DAY_SUNDAY = "sunday";

	//create db query
	static final String DB_CREATE = "create table " + ALARM_TABLE + "("
			+ ALARM_ID + " integer primary key autoincrement, "
			+ ALARM_ENABLED + " boolean, "
			+ ALARM_METHOD_NAME + " text, "
			+ ALARM_SOUND_PATH + " text, "
			+ ALARM_HOUR + " int, "
			+ ALARM_MINUTE + " int, "
			+ WEEK_DAY_MONDAY + " int, "
			+ WEEK_DAY_TUESDAY + " int, "
			+ WEEK_DAY_WEDNESDAY + " int, "
			+ WEEK_DAY_THURSDAY + " int, "
			+ WEEK_DAY_FRIDAY + " int, "
			+ WEEK_DAY_SATURDAY + " int, "
			+ WEEK_DAY_SUNDAY + " int"
			+ ");";

	public DBHelper(Context context) {
		super(context, DB_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// create table with columns
		Log.d("DATABASE", "CREATE TABLE");
		db.execSQL(DB_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//TODO Need release this method: public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
		//posible before release
		//recreate tables
		Log.d("DATABASE", "CREATE TABLE");
		db.execSQL("DROP "+ALARM_TABLE);
		db.execSQL(DB_CREATE);
	}

}