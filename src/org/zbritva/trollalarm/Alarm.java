package org.zbritva.trollalarm;

import java.io.Serializable;
import android.util.Log;


public class Alarm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5628180409357979869L;
	private long alarmID =-1;
	private boolean enabled = true;
	private String methodName;
	private String soundPath;
	private int hour;
	private int minute;
	private WeekDays week;
	public Alarm(){
		hour = 7;
		minute = 0;
		methodName = "SimpleShutdown";
		soundPath="";
		week = new WeekDays();
	}

	public long getID(){
		return alarmID;
	}

	public void setID(long id){
		alarmID = id;
	}

	public int getHour(){
		return hour;
	}

	public int getMinute(){
		return minute;
	}

	public boolean setMinute(int minute){
		if (minute >=0 && minute < 60){
			this.minute = minute;
			return true;
		}
		return false;
	}

	public boolean setHour(int hour){
		if (hour >=0 && hour < 24){
			this.hour = hour;
			return true;
		}
		return false;
	}

	public boolean isEnabled(){
		return enabled;
	}

	public void setEnabled(boolean enabled){
		this.enabled = enabled;
	}

	public WeekDays getWeek(){
		return week;
	}

	public void setWeek(WeekDays week){
		this.week = week;
	}

	public String getMethodName(){
		return methodName;
	}

	public void setMethodName(String methodName){
		this.methodName = methodName;
	}

	public void setSoundPath(String soundPath){
		this.soundPath = soundPath;
	}

	public String getSoundPath(){
		return soundPath;
	}

	@Override
	public boolean equals(Object o) {
		Log.d("EQUALS", "EQUALS ALARMS");
		if (super.equals(o) ==true){
			Alarm a = (Alarm) o;
			if (week.equals(a.week))
				if (this.enabled == a.enabled)
					if(this.hour == a.hour)
						if(this.methodName == a.methodName)
							if(this.minute ==a.minute)
								if(this.soundPath ==a.soundPath){
									Log.d("EQUALS", "EQUALS TRUE");
									return true;
								}
		}
		Log.d("EQUALS", "EQUALS FALSE");
		return false;
	}
}
